/*
    Welcome to your first C Program

    NEED TO INSTALL:
    To compile this, we are going to use GNU GCC
    - OSX users have this installed already
    - Windows users need to install and add to PATH
    - Linux users should easily be able to install using apt-get (Ubuntu) or similar

    TO RUN:
    Open up terminal, CMD or similar.
    Find this file in the terminal
    - Linux/OSX use 'cd' to change directory and 'ls' to list files
    - Windows use 'cd' to change director and 'dir' to list files
    
    COMPILE:
    Once at the file, run this line:
    gcc -ansi -W -pedantic -o main main.c

    This uses the GCC compiler to compile/link our main.c file.
    -ansi flag means we compile with the ANSI standard
    -W flag means spit out warnings
    -pedantic flag means be picky about our code 
    -o <file> flag means output to this file (aka the exe)
    main.c is our code file

    RUN:
    If you get no warnings, run the code with ./main or main.exe or ./main.exe
    depending on the os
*/

/* printf lives in this file*/
#include <stdio.h> 

int main(int argc, char* argv[])
{
    /* variables have to be delcared first in ANSI standard*/
    int i;
    char* text;

    /* Ex 1 variables */
    char character;
    short s1;
    int isecond;
    long l;
    float f;
    double d;

    /* Bonus task variables */
    unsigned char u_ch;
    unsigned int u_i;
    unsigned short u_s;
    unsigned long u_l;

    /* Super bonus task variable */
    float formattest;

    /*Exercise 1 */
    character = 64;
    s1 = 3;
    isecond = 27;
    l = 200000;
    f = 1.24;
    d = 1.22222;

    printf("Char: %c,\n", character);
    printf("Short: %hd,\n", s1);
    printf("Int: %d,\n", isecond);
    printf("Long: %ld,\n", l);
    printf("Float: %f,\n", f);
    printf("Double: %f,\n", d);

    /*Exercise 2 */
    u_ch = 67;
    u_i = 4096;
    u_s = 123;
    u_l = 999999999;

    printf("Unsigned char: %hhu,\n", u_ch);
    printf("Unsigned int: %u, \n", u_i);
    printf("Unsigned long: %lu, \n", u_l);
    printf("Unsigned short: %hu, \n", u_s);

    formattest = 1.2341234;

    printf("Formatted floating point: %1.4f\n", formattest);

    /* this is a C for loop, it is printing the arguments */
    for (i = 0; i < argc; i++) {
       printf("%s\n", argv[i]); 
    }

    /* trying running "./main arg1 1 lol" */

    /* this is a print format, \n is a new line */
    printf("Hello world!\n");

    /* lets use that char* text, which is basically a string */
    text = "Why did the chicken cross the road?";

    /* to print it, we use %s, similar to Java */
    printf("%s\n", text);

    /* you can pass it in as many times as you'd like */
    printf("%s, \n%s Who!? \n%s Wha!?\n", text, text, text);

    /* 
        Exercise 1:
        For the following types, 
        create one, at the top,
        and print them out using printf,
        the flags are called "Format parameters" and can be found on Google :)
        
        + char (it will look weird, unless you use ascii tables)
        + short
        + int
        + long
        + long long
        + float
        + double
    */

    /* e.g. %f is for float, printf("%f", myFloat); */

    /* oh, one more thing, C don't care about whitespace */
    printf(
        "This is perfectly valid as it really don\'t care\n"
    ); printf("Please keep your code neat!\n");

    /* we return */
    return 0;
}

/* 
    Bonus 
    ------------
    Find out how to create and print the unsigned version of:
    + char
    + short
    + int
    + long
    + long long

    Super Bonus
    ------------
    Find out how to print a float with 4 decimals after the point:
    i.e. 1.2341234 becomes 1.2341
*/
